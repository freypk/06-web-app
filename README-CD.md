Übung(en): Continous Integration & Delivery (CI/CD) mit Containern
==================================================================

Einfache Web Applikation - CD Erweiterung für Container

Konfiguration CI/CD
-------------------

Geht auf GitLab auf Eure Hauptgruppe z.B. `cdi-gitops`.

Wählt -> Settings -> CICD -> Variables an und erstellt folgende Variablen:

* CI_REGISTRY - neue Eures Repositories, siehe -> Packages & Registries -> Container CI_REGISTRY
* CI_REGISTRY_USER - Euer Username für GitLab
* CI_REGISTRY_PASSWORD - Euer Password für GitLab

Erweiterung Pipeline
--------------------

Erweitert Eure `.gitlab-ci.yaml` um einen neuen Stage `release` und den dazugehörenden Job:

    # Release als Container Image mit Tag
    release_job:
      stage: release
      # Use the official docker image.
      image: docker:latest
      services:
        - docker:dind
      before_script:
        - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" $CI_REGISTRY
      script:
        - docker build --pull -t "$CI_REGISTRY_IMAGE:$CI_COMMIT_TAG" .
        - docker push "$CI_REGISTRY_IMAGE:$CI_COMMIT_TAG"
      artifacts:
          paths:
              - target    
      needs:
        - job: build_job
          artifacts: true  
      rules:
      - if: "$CI_COMMIT_TAG"

Repository Taggen
-----------------

Durch das Taggen des Repositories wird automatisch eine Pipeline gestartet (sonst manuell starten), die Applikation gebuildet und in der Container Registry abgestellt.
